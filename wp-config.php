<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'web' );

/** Database username */
define( 'DB_USER', 'web' );

/** Database password */
define( 'DB_PASSWORD', '123456' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{[Opp/&4:EzPUXs]q!y)&bu]{Ikyc0(I194#lGTvZB],g!8v$L-adaWL2Lj0y<fR' );
define( 'SECURE_AUTH_KEY',  'FdZmms</1(V<6igW^:@!KCzOBi<~t<s(Y~j>Z6/}K eK<Sgn(vk}yjSu>qPp_$-^' );
define( 'LOGGED_IN_KEY',    '^hZtoCY4 B[DTz]mys]C&Q-VB_o_D#W1M; ^rYL07N~zsC~>@b[iZ09_ZB,@I*5j' );
define( 'NONCE_KEY',        'tj[_D!&v%q45*2,ED;f{U4BKLkTh7h9oj.@;lg{YRe|x3/^$]{8V,K(1B-!W m83' );
define( 'AUTH_SALT',        'x=8#Ms|nTn~gZd`x3];{tBy@^mI2HLmrEixP?}[RXX.4xukT QDEG)CBj1uR1!TO' );
define( 'SECURE_AUTH_SALT', 'cC;jVGDr0Z?WK:98t)R0XH=nvftT9w4(>MxcyhmwNg P s)MtOZYlw!Ose_fW!`e' );
define( 'LOGGED_IN_SALT',   'R|-?@C;uKV_FR&d6A0{2g|kfi_CP_4>V~B7-J+65NcxYRA@%K3^m(YY#|E6}Rlj]' );
define( 'NONCE_SALT',       '$-}~F}f%K-+ZOYGMuw?M*CXA3p%HUVxr=eaJWDyi!:^YVnx &QZIxyajiH^H %-~' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
